﻿'use strict';

let users = {};

let createButton = document.querySelector('#createButton');
let renameButton = document.querySelector('#renameButton');
let nameField = document.querySelector('#lastName');
let usersTable = document.querySelector('#usersTable');
let usersTableTbody = usersTable.querySelector('tbody');

let generateButton = document.querySelector('#generateButton');
let countField = document.querySelector('#countUsers');

let currentId = null; // Текущий айди для rename кнопки
let currentCard = null; // Текущая нода карточки для rename кнопки



createButton.addEventListener('click', function(event) { //Ивент для кнопки создания пользователя
	event.preventDefault();

	if (!nameField.value) { // Проверка ввода
		alert('Ошибка ввода!');
		return 0;
	}

  let nameValue = nameField.value; // Записываем имя в переменную с инпута

	let userHash = generateUserHash(); // Создаем уникальный айди пользователя

  users[userHash] = User(); // Делаем из этого айди объект с помощью конструктора
  users[userHash].addUser(nameValue, userHash); // Вызываем публичный метод создания пользователя в объекте передав туда его имя и айди

  nameField.value = ''; // Очищаем input
});

renameButton.addEventListener('click', function(event) { //Ивент для кнопки изменения имени
	event.preventDefault();

	if (!nameField.value) { // Проверка ввода
		alert('Ошибка ввода!');
		return 0;
	}

	let changedName = nameField.value; // Записываем значение инпута в переменную

	users[currentId].renameUser(changedName, currentCard); // Вызываю публичный метод изменения имени для текущего пользователя

	currentId = null; // Обнуляем текущий айди для rename кнопки
	currentCard = null; // Обнуляем текущую ноду карточки для rename кнопки

	renameButton.style.display = 'none';
	createButton.style.display = 'inline-block';
	nameField.value = ''; // Очищаем input
});

generateButton.addEventListener('click', function(event) { //Ивент для кнопки генерации пользователей
	event.preventDefault();

	if (!countField.value || countField.value < 1 || !isNumeric(countField.value)) { // Проверка ввода
		alert('Ошибка ввода!');
		return 0;
	}

	initUsers(countField.value); // Вызываем ф-цию генерации пользователей

	countField.value = ''; // Очищаем input
});

function User() { // Конструктор пользователя
  let userName = '';
  let userId = null;
  let card = document.createElement('tr');
  let removeBtn = document.createElement('button');
  let textUserName = document.createElement('td');
  textUserName.classList.add('align-middle');
	let renameBtn = document.createElement('button');

  function createCard() {
    card.id = userId;

    let tableNumber = document.createElement('th');
    tableNumber.classList.add('align-middle');
    card.appendChild(tableNumber);

		textUserName.textContent = userName;
    card.appendChild(textUserName);


    renameBtn.classList.add('btn', 'btn-warning');
    renameBtn.textContent = 'Edit';
    let renameBtnTd = document.createElement('td');
    renameBtnTd.appendChild(renameBtn);
    card.appendChild(renameBtnTd);

    removeBtn.classList.add('btn', 'btn-danger');
    removeBtn.textContent = 'Delete';
    let removeBtnTd = document.createElement('td');
    removeBtnTd.appendChild(removeBtn);
    card.appendChild(removeBtnTd);

    let renameCallback = function(event) {
      event.preventDefault();

			createButton.style.display = 'none';
			renameButton.style.display = 'inline-block';
			nameField.value = userName;

			currentId = userId; // Задаем текущий айди для rename кнопки
			currentCard = document.querySelector('#' + userId); // Задаем текущую ноду карточки для rename кнопки
    };

    let removeCallback = function(event) {
			event.preventDefault();

			let delCard = this.parentNode.parentNode; // Карточка, которую удаляем

			users[delCard.id].removeUser(delCard); // Вызываем её публичный метод удаления в нужной карточке, передав её ноду из разметки
    };

    removeBtn.addEventListener('click', removeCallback);
		renameBtn.addEventListener('click', renameCallback);

    usersTableTbody.appendChild(card);
    usersTable.style.display = 'table';
  }


  return {
    addUser: function(name, id) {
			this.name = name;
      userName = name;
      userId = id;
      createCard();
    },

    removeUser: function(card) {
			if (card) {
				card.remove(); // Удаляем из разметки карточку если её передали аргументом
			}

			delete users[userId]; // Удаляем из глобального объекта юзера по его айдишнику из замыкания
		},

		renameUser: function(name, card) {
			this.name = name;
			userName = name;

			if (card) {
				card.querySelector('td:nth-child(2)').textContent = name;
			}
		}
  };
}

function initUsers(count) { // Функция генерации случайных пользователей
	let names = ['Саша', 'Вася', 'Петя', 'Игорь', 'Коля', 'Маша', 'Вика', 'Кирилл', 'Женя', 'Света', 'Вова']; //Массив имен для рандомной генерации пользователя

	for (let i = 0; i < count; i++) { // Запускаем цикл создание пользователей
		let randName = names[ generateRandNum(0, names.length - 1) ]; // Берем случайное имя из массива namess
		let userHash = generateUserHash(); // Генерируем уникальный хеш

		users[userHash] = User();
		users[userHash].addUser(randName, userHash);
	}
}

function generateUserHash() { // Создает уникальный айди пользователя
	let date = new Date;
	date = date.getTime() + '';
	let hash = '' + Math.floor(Math.random()*10) + Math.floor(Math.random()*10) + Math.floor(Math.random()*10)
								+ Math.floor(Math.random()*10) + Math.floor(Math.random()*10)
								+ date.slice(-8); // Получаем уникальный хеш, где первые 5 чисел рандом, а последние 8 от текущей даты

  return 'user' + hash;
}

function generateRandNum(min, max) { // Возвращает случайное число от min до max включительно
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isNumeric(n) { // Проверка на число
  return !isNaN(parseFloat(n)) && isFinite(n);
}
