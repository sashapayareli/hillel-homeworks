'use strict';

var arr = [3, , 6, , , 7, , undefined, 34, , , , 97, , 43];
var searchIndex = prompt('Введите индекс: ', '');
var elem;

if ( searchIndex >= 0 
		 && searchIndex % 1 === 0
		 && searchIndex != ''
		 && searchIndex != null) { // Проверка ввода
	
	searchIndex = +searchIndex;		
	elem = arr[searchIndex];

	if ( arr.indexOf(elem, searchIndex) === searchIndex ) { // Если элемент существует
		console.log('Элемент существует это: ' + elem);

		arr.splice(searchIndex, 1); // Удаляем элемент
		arr.push(elem); // Закидываем в конец массива элемент

		console.log('Новый массив с этим элементом в конце: ');
		console.log(arr);
	} else {
		console.log('Элемента не существует!');
	}
} else {
	alert ('Не правильно ввели индекс. Перезагрузите страничку!')
}

