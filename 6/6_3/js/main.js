'use strict';

var items = []; // Исходный массив
var count = 0; // Ков-во элементов


console.log('Введите в консоль: createItem("Название_товара", "Цену", "Категорию_товара"), чтобы создать товар.');
console.log('Введите в консоль: showItems(), чтобы посмотреть исходный массив товаров');
console.log('Введите в консоль: filterPrice("От", "До"), чтобы отфильтровать цену товара.');
console.log('Введите в консоль: filterCategory("Категория"), чтобы отфильтровать товары по категории.');
console.log('Введите в консоль: findLengthCategory("Категория"), чтобы посчитать ков-во товаров в категории.');
console.log('Введите в консоль: deleteItem("Название_товара"), чтобы удалить товар.');
console.log('Введите в консоль: sortByPrice("+"), чтобы отсортировать товар по возрастанию или sortByPrice("-") по убыванию');
console.log('Введите в консоль: sortAndFilter("+", "Категория"), чтобы отсортировать и отфильтровать товар по возрастанию и категории или sortAndFilter("-", "От", "До"), чтобы отсортировать и отфильтровать товар по убыванию и цене');
console.log('Введите в консоль: filterAndSum("Категория"), чтобы отфильтровать по категории и получить сумму цен товаров. Либо filterAndSum("От", "До") чтобы отфильтровать по цене и получить сумму цен товаров');
console.log('----------------------Аргументы вводите всегда в кавичках ""---------------------------------------------');


// Уже заполненный массив, осталось только раскомментировать и работать с ним

// createItem("Монитор", "1500", "Техника");
// createItem("Стул", "10", "Мебель");
// createItem("Мышка", "47", "Техника");
// createItem("Кресло", "500", "Мебель");
// createItem("Клавиатура", "55", "Техника");
// createItem("Яблоко", "2", "Продукты");
// createItem("Диван", "700", "Мебель");



function createItem(name, price, category) { // 1. Напишите функцию, которая принимает название товара, его цену и категорию, добавляет этот товар в массив, где каждый товар это объект и возвращает массив всех товаров. Товаров может быть сколько угодно.
	if (!name || !isNumeric(price) || price <= 0 || !category ) { // Проверка ввода
		console.log('Не правильно написали аргументы функции. Попробуйте вызвать её еще раз!');
		return;
	}
	
	count++;
	var currentIndex = count - 1;

	items[currentIndex] = {};
	items[currentIndex].name = name;
	items[currentIndex].price = +price;
	items[currentIndex].category = category;

	return items;
}

function showItems() { // Вывести массив в консоль
	console.log(items);
}

function filterPrice(min, max, arr) { // 2. Напишите функцию, которая фильтрует товары по цене от и до и возращает новый массив только с товарами выбранного ценового диапазона или пустой массив.
	if (!isNumeric(min) || !isNumeric(max) || min < 0 || +max < +min) { // Проверка на ввод
		console.log('Не правильно написали аргументы функции. Попробуйте вызвать её еще раз!');
		return;
	}

	min = +min;
	max = +max;

	var newItems = (arr === undefined) ? items.filter(comparePrice) : arr.filter(comparePrice); // Если не передали массив аргументом, то фильтруем исходный

	return newItems;


	function comparePrice(value) { // Callback для фильтровки цены
		if (value.price >= min && value.price <= max) {
			return true;
		}
	}
}

function filterCategory(category, arr) { // 3. Напишите функцию, которая фильтрует товары по категории и возращает новый массив только с товарами выбранной категории, если она есть или пустой массив.
	if(!category) {
		console.log('Не правильно написали аргументы функции. Попробуйте вызвать её еще раз!');
		return;
	}

	var newItems = (arr === undefined) ? items.filter(compareCategory) : arr.filter(compareCategory); // Если не передали аргументом массив, то фильтруем исходный

	return newItems;


	function compareCategory(value) { // Callback
		if (value.category === category) {
			return true;
		}
	}
}

function findLengthCategory(category) { // 4. Напишите функцию, которая возвращает количесто товаров в категории.
	var newItems = filterCategory(category);
	return (newItems !== undefined) ? newItems.length : undefined;
}

function deleteItem(name) { // 5. Напишите функцию, которая удаляет товар по имени.
	if (!name) { // Проверка ввода
		console.log('Не правильно ввели имя товара!');
		return;
	}

	var isDeleted = false;

	items = items.filter(delItem);
	(isDeleted) ? console.log('Товар удален') : console.log('Товар не найден!');



	function delItem(value) { // Callbacks
		if (value.name === name) {
			isDeleted = true;
			return false;
		}

		return true;
	}
}

function sortByPrice(direction) { // 6. Напишите функции, которые сортируют товары по цене от меньшего к большему и наоборот и возвращают новый массив
	var newItems = [];
	

	switch (direction) {
		case '+':  // Если по возрастанию
			return newItems = items.sort(sortAscending);
		case '-': // Если по убыванию
			return newItems = items.sort(sortDescending);
		default : return console.log('Не правильно ввели аргумент функции! Попробуйте еще раз');
	}


	function sortAscending(a, b) { // Callback
		return a.price - b.price;
	}

	function sortDescending(a, b) { // Callback
		return b.price - a.price;
	}
}

function sortAndFilter(direction, value, secondValue) {// 7. Напишите функцию, котрая принимает вид сортировки (от большего к меньшему или наоборот) и фильтра (диапазон цены или категория) и возвращает новый массив товаров определённой выборки, отсортированные как указал пользователь.
	if ((direction !== '+' && direction !== '-') || !value) { // Проверка ввода
		console.log('Не правильно ввели аргументы функции! Попробуйте еще раз.');
		return;
	}

	var newItems = [];
	newItems = sortByPrice(direction); // Сортируем массив по цене

	if ( isNumeric(value) && isNumeric(secondValue) ) { // Если второй и третий аргумент числа
		return newItems = filterPrice(value, secondValue, newItems); // Фильтруем по цене
	} else if (!isNumeric(value) && secondValue === undefined) { // Если второй аргумент не число и третий не задали
		return newItems = filterCategory(value, newItems);// Фильтруем по категории
	} else {
		console.log('Не правильно ввели аргументы функции! Попробуйте еще раз.');
		return;
	}
}

function filterAndSum(value, secondValue) { // 8. Напишите функцию, которая принимает фильтр категорий и возвращает сумму цен товаров этой выборки.
	
	var newItems = [];
	
	if ( isNumeric(value) && isNumeric(secondValue) ) { // Если первый и второй аргумент числа
		newItems = filterPrice(value, secondValue); // Фильтруем по цене
	} else if (!isNumeric(value) && secondValue === undefined) { // Если первый аргумент не число и второй арг. не задали
		newItems = filterCategory(value);// Фильтруем по категории
	} else {
		console.log('Не правильно ввели аргументы функции! Попробуйте еще раз.');
		return;
	}

	if (newItems === undefined) { //Запороли ввод аргументов
		return;
	}

	var result = newItems.reduce(SumArr, 0);
	return result;


	function SumArr(sum, current) { //Callback
		return sum + current.price;
	}
}

function isNumeric(number) { // Проверка числа
	return !isNaN(parseFloat(number)) && isFinite(number);
}