'use strict';

var minCount = 3;
var maxCount = 10;
var maxNumber = 100;
var str = prompt('Введите числа через запятую и пробел, кол-во штук от ' + minCount + ' до ' + maxCount + ' и макс значение числа: ' + maxNumber);

var arr = (str === null) ? [] : str.split(', '); // Делаем из строки массив
arr = arr.map( strToNumArr ); // Форматируем его в числовой тип

if ( isValidArr(arr, minCount, maxCount, maxNumber) ) { // Если массив удовлетворяет входным условиям
	arr.sort(sortNumbers); //Сортируем по возрастанию чисел
	var result = arr.join(', '); // Делаем из массива строку
	console.log('Результат сортировки по возрастанию: ' + result);

	arr.sort(sortByEven); //Сортируем сначала четные числа
	var secondResult = arr.join(', '); // Делаем из массива строку
	console.log('Доп. задание*. Результат сортировки сначала четные числа: ' + secondResult);

} else {
	alert('Вы не правильно ввели данные по условию задачи! Перезагрузите страничку');
}



function strToNumArr(value) { // Функция для str.map, переводит строчный массив в числовой
	if (value === '') {
		return NaN;
	}

	return +value;
}

function isValidArr(arr, minCount, maxCount, maxNumber) { // Функция проверяет лежат ли числа в массиве и не больше ли они макс числа, и к-ва
	if (arr.length < minCount || arr.length > maxCount) { // Проверка на кол-во элементов
		return false;
	}

	if ( !arr.every(isValidNum) ) { //Проверка на наличие чисел и максимального значения
		return false;
	}

	return true;
}

function isValidNum(number) { // Функция для arr.every(), проверяет является ли элемент массива числом и не больше макс. знач.
	return ( (!isNaN(parseFloat(number)) && isFinite(number)) 
					 && number <= maxNumber );
}

function sortNumbers(a, b) { // Функция для arr.sort(), сортировка по возрастанию чисел
	return a - b;
}

function sortByEven(a, b) { // Функция для arr.sort(), cравнивает два числа. Четные меньше чем нечетные
	if (a % 2 === 0 && b % 2 === 0) { // Если а четное и b тоже
		return a - b;
	}

	if (a % 2 === 0 && b % 2 !== 0) { // Если а четное и b нечетное
		return -1;
	}

	if (a % 2 !== 0 && b % 2 === 0) { // По аналогии
		return 1;
	}

	if (a % 2 !== 0 && b % 2 !== 0) { // По аналогии
		return a - b;
	}
}