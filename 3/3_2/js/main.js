'use strict';

var massPound = prompt('Please type mass in pound. Fractional numbers enter with dot, e.g: 3.44', '');
var massToKg = +massPound / 2.20462; //Перевод массы в кг
var moneyToPound = 0.0288; // Фунтов за 1 грн

if (+massPound >= 0 && massPound !== '') { // Проверка на правильность ввода
	massToKg = Number(massToKg);

	if(massToKg <= 5) {
		alert('From you ' + (3 * moneyToPound * massToKg) + ' pounds');
	} else if (massToKg <= 10) {
		alert('From you ' + (5 * moneyToPound * massToKg) + ' pounds');
	} else if (massToKg <= 15) {
		alert('From you ' + (10 * moneyToPound * massToKg) + ' pounds');
	} else {
		alert('Your mass more then 15 kg! Leave something and reload the page')
	}

} else {
	alert('You entered the wrong mass, reload the page!');
}