'use strict';

let dropdownItems = document.querySelectorAll('.main-nav__item--dropdown'); // Ищу все li, которые имеют вложенные списки
let currentDropdown = null; // Текущего развернутого списка пока нет

for (let i = 0; i < dropdownItems.length; i++) { // Вешаю eventListener на каждую li
	dropdownItems[i].addEventListener('click', dropdownCallBack);
}

function dropdownCallBack(evt) {
	let dropdownList = this.querySelector('.main-nav__dropdown'); // Находим спрятанный список в текущей li 
	dropdownList.classList.toggle('main-nav__dropdown--show'); // Показываем его

	switch (currentDropdown) { // Меняем текущий указатель на развернутый список в зависимости от ситуации
		case null: { // Если развернутого списка еще нет
			currentDropdown = this;
			break;
		}

		case this: { // Если кликнули по текущему развернутому списку
			currentDropdown = null;
			break;
		}

		default: { // Если кликнули по другому развернутому списку
			let prewDropdown = currentDropdown.querySelector('.main-nav__dropdown--show');
			prewDropdown.classList.remove('main-nav__dropdown--show');
			currentDropdown = this;
			break;
		}
	}
}