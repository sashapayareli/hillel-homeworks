'use strict';

var modalBtn = document.querySelector('.modal-show-btn'); // Ищу кнопку вызова модального окна
var modalWindow = document.querySelector('.modal-window'); // Ищу модальное окно
var overlay = document.querySelector('.overlay'); // Ищу оверлей
var closeModalBtn = document.querySelector('.modal-window-close'); // Ищу кнопку закрытия модального окна

modalBtn.addEventListener('click',modalCallBack); // Вешаю событие на кнопку вызова модального окна

overlay.addEventListener('click', modalCallBack); // Вешаю событие на оверлей

closeModalBtn.addEventListener('click', modalCallBack); // Вешаю событие на кнопку закрытия модального окна

function modalCallBack(evt) {
	overlay.classList.toggle('overlay-show'); // Переклячаю классы, скрыть - показать оверлей
	modalWindow.classList.toggle('modal-window-show'); // Переклячаю классы, скрыть - показать модальное окно
}