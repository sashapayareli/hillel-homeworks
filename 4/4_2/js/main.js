'use strict';

var number = prompt('Введите число', '');

if ((number <= 0 || number > 0) && number !== '') { // Проверка на правильность ввода числа
	number = Number(number);

	var result = isSimple(number);

	if (result) {
		console.log('Число простое');
	} else {
		console.log('Число не простое');
	}

} else {
	alert('Вы ввели не правильно число!');
}



function isSimple(number) { // Функция, которая проверяет число простое или нет
	var isSimpleNumber = (number > 1) ? true : false; // По умолчанию считаем, что число простое, если оно больше 1

	for (var i = number - 1; i != 1 && isSimpleNumber; i--) {
		if (number % i == 0) { // Если нету остатка от деления числа на счетчик в промежутке от 2 до введенного числа, то число не простое 
			isSimpleNumber = false;
		}
	}

	return isSimpleNumber;
}