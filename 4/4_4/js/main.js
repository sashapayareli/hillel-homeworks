'use strict'

var singleLine = ''; // Пустая строка для конкатенации

do {
	var input = prompt('Введите название фрукта: ', '');
	var isError; // Если не прошел проверку на ввод

	if (input && !(input >= 0 || input < 0)) { // Если строка не пустая, не null И не число
		singleLine += input + ' ';
	} else if (input != null){
		isError = true;
		alert('Ошибка ввода!');
	}
	
} while(input != null && isError != true); // Пока не отменили ввод или не запороли его

(singleLine) ? console.log(singleLine) : console.log('Пусто!');