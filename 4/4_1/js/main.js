'use strict';

var number = prompt('Введите число', '');
if (number >= 0 && number !=='') { // Проверка на правильность ввода
	number = Number(number);
	
	var sqrtNumber = calcSqrt(number);
	console.log('Корень квадратный: ' + sqrtNumber);

	var result = showNumbers(sqrtNumber);
	console.log('Результат: ' + result)

} else {
	alert('Вы ввели не правильно число, перезагрузите страничку!');
}



function calcSqrt(number) { // Функция нахождения корня квадратного
	var lastSqrt;

	for ( var sqrt = 0; sqrt * sqrt <= number; sqrt++) { //Если квадрат sqrt получился больше введеного числа, то возвращаем результат предыдущей итерации, lastSqrt
		lastSqrt = sqrt;
	}

	sqrt = lastSqrt;

	for ( ; sqrt * sqrt <= number; sqrt += 0.1) { // Находим дробную часть
		lastSqrt = sqrt;
	}

	return lastSqrt.toFixed(1);
}

function showNumbers(number) { // Функция, которая находит все целые числа меньше number, от 1 до 100
		var minNumber = 1; // от 1
		var maxNumber = 100; // до 100
		var singleLine = ''; // Нужна для конкатенации строк 

		while (minNumber <= maxNumber && minNumber < number) { //Если счетчик меньше 100 и меньше корня квадратного, сконкатенировать строки и увеличить счетчик на 1 до тех пор, пока условие не нарушится.
			singleLine += minNumber + ' ';
			minNumber++;
		}

		return singleLine;
	}