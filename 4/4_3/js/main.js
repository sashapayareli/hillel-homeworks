'use strict';

var a = prompt('Введите первое число: ', '');
var b = prompt('Введите второе число: ', '');
var c = prompt('Введите третее число: ', '');
var compare = prompt('Введите + если хотите увидеть большее число или - если меньшее: ', '');

if(((a >= 0 || a < 0) && a !=='') // Проверка правильности ввода
&& ((b >= 0 || b < 0) && b !=='') 
&& ((c >= 0 || c < 0) && c !=='') 
&& (compare === '+' || compare === '-')) {

	a = Number(a);
	b = Number(b);
	c = Number(c);

	var min = FindMin(a, b, c);
	var max = findMax(a, b, c);

	(compare === '+') ? console.log('Большее число: ' + max) : console.log('Меньшее число: ' + min);
} else {
	alert('Вы не правильно ввели входные данные, перезагрузите страничку!');
}



function FindMin (a, b, c) { // Функция нахождения минимального числа
	var min;
	if (a <= b && a <= c) {
		min = a;
	} else if (b <= a && b <= c) {
		min = b;
	} else {
		min = c;
	}

	return min;
}

function findMax (a, b, c) { // Функция нахождения максимального числа
	var max;
	if (a >= b && a >= c) {
		max = a;
	} else if (b >= a && b >= c) {
		max = b;
	} else {
		max = c;
	}

	return max;
}