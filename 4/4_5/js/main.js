'use strict';

var oldEquivalent = 27.724; // Старый курс 1 доллар = 27.724 грн

var Equivalent = prompt('Введите новый курс за 1 доллар в гривнах: ', '');
var cash = prompt('Сколько у вас долларов?: ', '');
var cashDifference; // Разница денег между сменой курса

if (Equivalent > 0 && cash > 0) { // Проверка на правильность ввода
	
	if (Equivalent > oldEquivalent) {
		cashDifference = Equivalent*cash - oldEquivalent*cash;
		console.log('Вы ушли в плюс на ' + cashDifference + ' гривен!');
	} else if (Equivalent < oldEquivalent) {
		cashDifference = oldEquivalent*cash - Equivalent*cash;
		console.log('Вы ушли в минус на ' + cashDifference + ' гривен!');
	} else {
		console.log('Курс не изменился!');
	}

} else {
	alert('Вы не правильно ввели данные!');
}