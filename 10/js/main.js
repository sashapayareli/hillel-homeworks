'use strict';

console.log('Введите в консоль: parse("№ части", "№ главы", "Вид стиля")\n Всего есть 4 стиля:\n "number" - нумерацией\n "dash" - дефисами\n "current" - нумерациями, как в учебнике\n "table" - таблицой');

function parse(part, chapter, style) {
	if (!isNumeric(part) || part <= 0 || part % 1 !== 0
	|| !isNumeric(chapter) || chapter <= 0 || chapter % 1 !== 0
	|| (style !== 'number' && style !== 'dash' && style !== 'current' && style !== 'table')) { // Проверка ввода
		console.error('Ошибка ввода!');
		return;
	}

	var parts = document.querySelectorAll('.list'); // Выбираю все части учебника

	if (part > parts.length) {
		console.error('Такой части учебника не существует!');
		return;
	}

	var requiredPart = parts[part - 1]; // Записываю нужную часть

	var chapters = requiredPart.querySelectorAll('.list__item'); // Выбираю все главы учебника нужной части

	if (chapter > chapters.length) {
		console.error('Такой главы учебника не существует!');
		return;
	}

	var requiredChapter = chapters[chapter - 1]; // Записываю нужную главу
	var nameChapter = requiredChapter.querySelector('.list__link').innerHTML; // Записываю название нужной главы
	var chapterThemesLinks = requiredChapter.querySelectorAll('.list-sub__link'); // Выбираю все темы нужной главы

	var chapterThemes = [];

	for (var i = 0; i < chapterThemesLinks.length; i++) {
		chapterThemes.push(chapterThemesLinks[i].innerHTML); // Прохожусь циклом по всем темам, и записываю в массив внутренний текст каждой темы
	}

	var list = {};

	for (var i = 0; i < chapterThemes.length; i++) {
		list[chapter + '.' + (i + 1)] = chapterThemes[i]; // Записываю в объект № главы по учебнику: тема
	}

	displayParse(list, nameChapter, style); // Вывожу объект в консоль
}

function displayParse(list, title, style) {
	console.log('Название главы: ' + title);

	switch (style) {
		case 'number' : {
			var i = 1;
			for (var key in list) {
				console.log(i + '. ' + list[key]);
				i++;
			}
			break;
		}

		case 'dash' : {
			for (var key in list) {
				console.log('- ' + list[key]);
			}
			break;
		}

		case 'current' : {
			for (var key in list) {
				console.log(key + ' ' + list[key]);
			}
			break;
		}

		case 'table' : {
			console.table(list);
			break;
		}
	}
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}