'use strict';

var result = f (3, 3, function () { //Пример вызова функции в которой 3 аргумента
	var str = 'Hello';
	return str;
});

console.log(result);



function f (a, b, c) {

	function sum (a, b) {
		return a + b;
	}

	if (a == undefined && b == undefined) { //2.1. Если агрументы a и b не переданы, они равны по умолчанию 2 и 3 соответсвенно.
		a = 2;
		b = 3;
	}

	if (typeof c === 'function') { //2.2. Если аргумент 'c' передан и он является функцией, то он выполняется после вызова функции sum.
		sum (a, b);
		return c();
	} else {
		return sum (a, b); //2.3. Функция f должны возвращать результат функции аргумента c, если он есть, либо результат функции sum.
	}
}
