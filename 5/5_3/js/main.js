'use strict';

var count = 0; // Изначальное кол-во товаров
var maxCount = 10; // Максимальное кол-во товаров без учета 0-го индекса
var items = {}; // Объект товаров

console.log('Введите в консоль createItem(), чтобы создать новый товар');
console.log('Введите в консоль findCategory("название_категории"), чтобы отсортировать и вывести объект отсортированных товаров');
console.log('Введите в консоль calcPrice("название_категории"), чтобы высчитать цену отсортированных товаров со скидкой и вывести её в консоль');


function createItem() { // Функция, которая создает объект товара
	if (count >= maxCount) {
		alert('Больше ' + maxCount + ' товаров нельзя!');
		return;
	}	
	
	var index = prompt('Введите индекс товара, максимум ' + maxCount + ' товаров!', '');

	if (index >= 0 && isNumeric(index)) { // Проверка на ввод

		if (index in items) { // Если уже существует такой индекс
			alert('Индекс уже занят! Введите функцию заново в консоль!');
			return;
		}

		index = Number(index);
		count++;
	
	} else {
		alert('Ошибка ввода! Введите функцию заново в консоль!');
		return;
	}

	items[index] = {}; // № товара, предоставляет собой тоже объект
	items[index].name = prompt('Введите название товара. Например: Ручка', ''); // Название товара

	do {

		if (items[index].name !== '' && items[index].name != null) {
			var isErrorInput = false;
		} else {
			isErrorInput = true;
			items[index].name = prompt('Вы не правильно ввели название! Введите название товара повторно. Например: Ручка', '');
		}

	} while(isErrorInput !== false);

	items[index].price = prompt('Введите цену товара. Например: 10', ''); // Цена товара

	do {

		if ( items[index].price > 0 && isNumeric(items[index].price) ) {
			items[index].price = Number(items[index].price);
			isErrorInput = false;
		} else {
			isErrorInput = true;
			items[index].price = prompt('Вы не правильно ввели цену! Введите цену товара повторно. Например: 10', '');
		}

	} while(isErrorInput != false);

	items[index].category = prompt('Введите категорию товара. Например: Канцтовары', ''); // Категория товара

	do {

		if (items[index].category !== '' && items[index].category != null) {
			isErrorInput = false;
		} else {
			isErrorInput = true;
			items[index].category = prompt('Вы не правильно ввели категорию! Введите название категории повторно. Например: Канцтовары', '');
		}

	} while(isErrorInput != false);

	return items;
}

function findCategory(category) { //Функция, которая сортирует по категории старый 'массив' объектов в новыйs
	var sortItems = {};

	for (var key in items) { // Прогоняем все подобъекты главного объекта
		if (items[key].category === category) { // Если категория запрашиваемая совпала - создаем новый подобъект в sortItems
			sortItems[key] = {};
			sortItems[key].name = items[key].name;
			sortItems[key].price = items[key].price;
			sortItems[key].category = items[key].category;
		}
	}

	if (Object.keys(sortItems).length === 0) { // Если сортировка пустая
		alert('Товаров не найдено!');
		return null;
	}

	return sortItems;
}

function calcPrice(category) { //Функция, которая считает общую цену
	var sortItems = findCategory(category);

	if (sortItems === null) { // Если сортировка пустаяs
		return;
	}

	var sum = 0;

	for (var key in sortItems) {
		sum += sortItems[key].price;
	}
	
	calcDiscount(sum);
}

function calcDiscount(number) { // Функция, которая высчитывает и выводит скидку
	if (number > 5) {
		var result = (number - (number / 100 * 12)).toFixed(2);
		console.log('Со скидкой вышло: ' + result);
	} else {
		result = number.toFixed(2);
		console.log('Без скидки вышло: ' + result);
	}
}

function isNumeric(n) { // Функция проверки на число
	return !isNaN(parseFloat(n)) && isFinite(n);
}