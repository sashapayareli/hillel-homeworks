﻿'use strict';

//Дом элементы
let mainAtmMess = document.querySelector('#atmMess');
let atmUserInput = document.querySelector('#atmUserInput');

let leftAtmMsg1 = document.querySelector('#leftAtmMsg1');
let leftAtmMsg2 = document.querySelector('#leftAtmMsg2');
let leftAtmMsg3 = document.querySelector('#leftAtmMsg3');
let leftAtmMsg4 = document.querySelector('#leftAtmMsg4');

let rightAtmMsg1 = document.querySelector('#rightAtmMsg1');
let rightAtmMsg2 = document.querySelector('#rightAtmMsg2');
let rightAtmMsg3 = document.querySelector('#rightAtmMsg3');
let rightAtmMsg4 = document.querySelector('#rightAtmMsg4');

let leftAtmBtn1 = document.querySelector('#leftAtmBtn1');
let leftAtmBtn2 = document.querySelector('#leftAtmBtn2');
let leftAtmBtn3 = document.querySelector('#leftAtmBtn3');
let leftAtmBtn4 = document.querySelector('#leftAtmBtn4');

let rightAtmBtn1 = document.querySelector('#rightAtmBtn1');
let rightAtmBtn2 = document.querySelector('#rightAtmBtn2');
let rightAtmBtn3 = document.querySelector('#rightAtmBtn3');
let rightAtmBtn4 = document.querySelector('#rightAtmBtn4');

let mainAtmBtn0 = document.querySelector('#mainAtmBtn0');
let mainAtmBtn1 = document.querySelector('#mainAtmBtn1');
let mainAtmBtn2 = document.querySelector('#mainAtmBtn2');
let mainAtmBtn3 = document.querySelector('#mainAtmBtn3');
let mainAtmBtn4 = document.querySelector('#mainAtmBtn4');
let mainAtmBtn5 = document.querySelector('#mainAtmBtn5');
let mainAtmBtn6 = document.querySelector('#mainAtmBtn6');
let mainAtmBtn7 = document.querySelector('#mainAtmBtn7');
let mainAtmBtn8 = document.querySelector('#mainAtmBtn8');
let mainAtmBtn9 = document.querySelector('#mainAtmBtn9');

let atmBtnEnter = document.querySelector('#atmBtnEnter');
let atmBtnClear = document.querySelector('#atmBtnClear');
let atmBtnCancel = document.querySelector('#atmBtnCancel');

let atmChangeForm = document.querySelector('#atmChangeForm');
let selectAtm = document.querySelector('#selectAtm');
let changeAtmBtn = document.querySelector('#changeAtmBtn');

let cardsInfo = document.querySelector('#cardsInfo');
let cardsModal = document.querySelector('#cardsModal');
let overlay = document.querySelector('#overlay');
let refreshCardsBtn = document.querySelector('#refreshCardsBtn');
let closeCardsModalBtn = document.querySelector('#closeCardsModalBtn');
let cardsTable = document.querySelector('#cardsTable');

//Глобальные переменные
let atms = [];
let users = [
  {
  id: 1,
  phoneNumber: '0667658943',
  cards: [ {cardNumber: '1111111111111111', commission: 3, credit: true, limit: 3000, balance: -1479} ]
  },
  {
    id: 2,
    phoneNumber: '0667658941',
    cards: [ {cardNumber: '4356374609651651', commission: 2, credit: false, balance: 500},
            {cardNumber: '4356374609641671', commission: 1, credit: true, limit: 400, balance: 435},
            {cardNumber: '4444444444444444', commission: 1, credit: false, balance: 1200} ]
  },
  {
    id: 3,
    phoneNumber: '0667651143',
    cards: [ {cardNumber: '1111111111111122', commission: 4, credit: true, limit: 3000, balance: -1400} ]
  },
  {
    id: 4,
    phoneNumber: '0117658941',
    cards: [ {cardNumber: '4356373339641671', commission: 6, credit: false, balance: 5000},
            {cardNumber: '4356374609633371', commission: 4, credit: false,  balance: 350},
            {cardNumber: '3334444444444444', commission: 11, credit: false, balance: 210} ]
  }
  ];

let atmPrevWindow = null;
let atmCurrentWindow = null;
let currentCard = null;
let currentAtm = null;

let codeLength = 16;
let phoneLength = 10;

//Класс CreateATM
class СreateATM {
  constructor(id, name, address, commission) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.commission = commission;

    atms.push(this);
    refreshAvailableAtm();
  }



  //События на всякие кнопки банкомата:

  //Переиспользуемые события в разных окнах банкомата:
  _btnsCodeListener(evt) {
    evt.preventDefault();

    if (atmUserInput.textContent.length === codeLength) {
      return;
    }

    atmUserInput.textContent += evt.target.textContent;
  }

  _clearCodeListener(evt) {
    evt.preventDefault();

    let userInput = atmUserInput.textContent;

    if (userInput.length === 0) {
      return;
    }

    atmUserInput.textContent = userInput.slice(0, userInput.length - 1);
  }

  _goToOptionsListener(evt) {
    evt.preventDefault();

    this._showOptionsScreen();
  }

  //События для окна ошибки банкомата:
  _continueErrorListener(evt) {
    evt.preventDefault();

    switch (atmPrevWindow) {
      case 'startScreen': {
        this._showStartScreen();
        break;
      }

      case 'changePhoneScreen': {
        this._showChangePhoneScreen();
        break;
      }

      case 'withdrawScreen': {
        this._showWithdrawScreen();
        break;
      }
    }
  }

  //События для окна старта банкомата:
  _enterCodeListener(evt) {
    evt.preventDefault();

    let userInput = atmUserInput.textContent;

    if (userInput.length !== codeLength) {
      this._showErrorScreen(`Код должен быть ${codeLength} символов!`);
      return;
    }

    if (!this._isExistingCode(userInput)) {
      this._showErrorScreen('Код не действителен!');
      return;
    }

    currentCard = userInput;
    this._showOptionsScreen();
  }

  //События для окна выбора действий банкомата(меню):
  _showBalanceListener(evt) {
    evt.preventDefault();

    this._showBalanceScreen();
  }

  _endWorkListener(evt) {
    evt.preventDefault();

    this._showStartScreen();
  }

  _changePhoneListener(evt) {
    evt.preventDefault();

    this._showChangePhoneScreen();
  }

  _withdrawListener(evt) {
    evt.preventDefault();

    this._showWithdrawScreen();
  }

  //События для окна снятия наличных банкомата:
  _withdrawCashListener(evt) {
    evt.preventDefault();

    let amount;
    let targetId = evt.target.id;

    switch (targetId) {
      case 'leftAtmBtn1': {
        amount = 50;
        break;
      }

      case 'leftAtmBtn2': {
        amount = 100;
        break;
      }

      case 'leftAtmBtn3': {
        amount = 150;
        break;
      }

      case 'leftAtmBtn4': {
        amount = 200;
        break;
      }

      case 'rightAtmBtn1': {
        amount = 250;
        break;
      }

      case 'rightAtmBtn2': {
        amount = 500;
        break;
      }

      case 'rightAtmBtn4': {
        amount = +atmUserInput.textContent;
        break;
      }
    }

    if (!this._isValidAmount(amount)) {
      this._showErrorScreen('Сумма должна быть кратная 50!');
      return;
    }

    if (!this._isEnoughBalance(currentCard, amount)) {
      this._showErrorScreen('Недостаточно средств/превышен лимит!');
      return;
    }

    this.withdrawCash(currentCard, amount);
    this._showSuccessScreen('Деньги сняты успешно!')
  }

  //События для окна смены номера телефона банкомата:
  _enterPhoneCodeListener(evt) {
    evt.preventDefault();

    let userInput = atmUserInput.textContent;

    if (userInput.length !== phoneLength) {
      this._showErrorScreen(`Код должен быть ${phoneLength} символов!`);
      return;
    }

    this.changePhone(currentCard, userInput);
    this._showSuccessScreen('Номер изменен!');
  }





  //Методы для переключения окон в банкомате:

  //Окно ошибки банкомата
  _showErrorScreen(message) {
    this._hideExcess();
    this._removeListeners();

    atmPrevWindow = atmCurrentWindow;
    atmCurrentWindow = 'errorScreen';

    mainAtmMess.textContent = message;
    rightAtmMsg4.textContent = 'ОК';

    this._continueErrorListener = this._continueErrorListener.bind(this);
    atmBtnEnter.addEventListener('click', this._continueErrorListener);
    atmBtnCancel.addEventListener('click', this._continueErrorListener);
    rightAtmBtn4.addEventListener('click', this._continueErrorListener);
  }

  //Окно успешной операции
  _showSuccessScreen(message) {
    this._hideExcess();
    this._removeListeners();

    atmCurrentWindow = 'successScreen';

    mainAtmMess.textContent = message;
    rightAtmMsg4.textContent = 'Вернуться в меню';

    this._goToOptionsListener = this._goToOptionsListener.bind(this);
    atmBtnEnter.addEventListener('click', this._goToOptionsListener);
    atmBtnCancel.addEventListener('click', this._goToOptionsListener);
    rightAtmBtn4.addEventListener('click', this._goToOptionsListener);
  }

  //Начальное окно ввода номера карточки
  _showStartScreen() {
    this._hideExcess();
    this._removeListeners();

    atmPrevWindow = null;
    currentCard = null;
    currentAtm = this;
    atmCurrentWindow = 'startScreen';

    mainAtmMess.innerHTML = `Здравствуйте!<br> Введите номер карточки:`;
    rightAtmMsg4.textContent = `Продолжить`;

    mainAtmBtn0.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn1.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn2.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn3.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn4.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn5.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn6.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn7.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn8.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn9.addEventListener('click', this._btnsCodeListener);

    atmBtnClear.addEventListener('click', this._clearCodeListener);

    this._enterCodeListener = this._enterCodeListener.bind(this);
    rightAtmBtn4.addEventListener('click', this._enterCodeListener);
    atmBtnEnter.addEventListener('click', this._enterCodeListener);
  }

  //Окно выбора действий банкомата
  _showOptionsScreen() {
    this._hideExcess();
    this._removeListeners();

    atmCurrentWindow = 'optionsScreen';

    mainAtmMess.textContent = 'Выберите операцию:';
    leftAtmMsg3.textContent = 'Баланс';
    leftAtmMsg4.textContent = 'Выдача наличных';
    rightAtmMsg3.textContent = 'Сменить номер';
    rightAtmMsg4.textContent = 'Закончить работу';

    this._showBalanceListener = this._showBalanceListener.bind(this);
    leftAtmBtn3.addEventListener('click', this._showBalanceListener);

    this._withdrawListener = this._withdrawListener.bind(this);
    leftAtmBtn4.addEventListener('click', this._withdrawListener);

    this._changePhoneListener = this._changePhoneListener.bind(this);
    rightAtmBtn3.addEventListener('click', this._changePhoneListener);

    this._endWorkListener = this._endWorkListener.bind(this);
    rightAtmBtn4.addEventListener('click', this._endWorkListener);
  }

  //Окно просмотра баланса
  _showBalanceScreen() {
    this._hideExcess();
    this._removeListeners();

    atmCurrentWindow = 'balanceScreen';

    let balance = this.checkBalance(currentCard);

    mainAtmMess.innerHTML = `На вашем балансе:<br> ${balance} грн.`;
    rightAtmMsg4.textContent = 'Назад';

    this._goToOptionsListener = this._goToOptionsListener.bind(this);
    rightAtmBtn4.addEventListener('click', this._goToOptionsListener);
    atmBtnCancel.addEventListener('click', this._goToOptionsListener);
  }

  //Окно смены номера телефона
  _showChangePhoneScreen() {
    this._hideExcess();
    this._removeListeners();

    atmCurrentWindow = 'changePhoneScreen';

    mainAtmMess.textContent = `Введите новый номер телефона:`;
    rightAtmMsg3.textContent = `Назад`;
    rightAtmMsg4.textContent = `ОК`;

    mainAtmBtn0.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn1.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn2.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn3.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn4.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn5.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn6.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn7.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn8.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn9.addEventListener('click', this._btnsCodeListener);

    atmBtnClear.addEventListener('click', this._clearCodeListener);

    this._goToOptionsListener = this._goToOptionsListener.bind(this);
    rightAtmBtn3.addEventListener('click', this._goToOptionsListener);
    atmBtnCancel.addEventListener('click', this._goToOptionsListener);

    this._enterPhoneCodeListener = this._enterPhoneCodeListener.bind(this);
    rightAtmBtn4.addEventListener('click', this._enterPhoneCodeListener);
    atmBtnEnter.addEventListener('click', this._enterPhoneCodeListener);
  }

  //Окно снятия наличных
  _showWithdrawScreen() {
    this._hideExcess();
    this._removeListeners();

    atmCurrentWindow = 'withdrawScreen';

    mainAtmMess.innerHTML = `Введите сумму денег<br>кратную 50 грн. :`;

    leftAtmMsg1.textContent = '50 грн';
    leftAtmMsg2.textContent = '100 грн';
    leftAtmMsg3.textContent = '150 грн';
    leftAtmMsg4.textContent = '200 грн';

    rightAtmMsg1.textContent = `250 грн`;
    rightAtmMsg2.textContent = `500 грн`;
    rightAtmMsg3.textContent = `Назад`;
    rightAtmMsg4.textContent = `ОК`;

    mainAtmBtn0.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn1.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn2.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn3.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn4.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn5.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn6.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn7.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn8.addEventListener('click', this._btnsCodeListener);
    mainAtmBtn9.addEventListener('click', this._btnsCodeListener);

    atmBtnClear.addEventListener('click', this._clearCodeListener);

    this._goToOptionsListener = this._goToOptionsListener.bind(this);
    rightAtmBtn3.addEventListener('click', this._goToOptionsListener);
    atmBtnCancel.addEventListener('click', this._goToOptionsListener);

    this._withdrawCashListener = this._withdrawCashListener.bind(this);
    rightAtmBtn4.addEventListener('click', this._withdrawCashListener);
    atmBtnEnter.addEventListener('click', this._withdrawCashListener);
    leftAtmBtn1.addEventListener('click', this._withdrawCashListener);
    leftAtmBtn2.addEventListener('click', this._withdrawCashListener);
    leftAtmBtn3.addEventListener('click', this._withdrawCashListener);
    leftAtmBtn4.addEventListener('click', this._withdrawCashListener);
    rightAtmBtn1.addEventListener('click', this._withdrawCashListener);
    rightAtmBtn2.addEventListener('click', this._withdrawCashListener);
  }




  //Служебные методы:

  //Проверяет код карточки, существует ли в users
  _isExistingCode(code) {
    for (let i = 0; i < users.length; i++) {
      for (let j = 0; j < users[i].cards.length; j++) {
        if (users[i].cards[j].cardNumber == code) {
          return true;
        }
      }
    }

    return false;
  }

  //Проверяет валидность введеной суммы снятия наличных
  _isValidAmount(amount) {
    if (amount !== 0 && amount % 50 === 0) { // Кратно 50
      return true;
    }

    return false;
  }

  //Проверяет достаточно ли у пользователя денег и кредитного лимита
  _isEnoughBalance(code, amount) {
    for (let i = 0; i < users.length; i++) {
      for (let j = 0; j < users[i].cards.length; j++) {
        if (users[i].cards[j].cardNumber == code) { //Нашли нужного пользователя
          let totalCommission = users[i].cards[j].commission + this.commission; //Посчитал общую комиссию
          let totalAmount = amount + (amount / 100 * totalCommission); //Посчитал выводимую сумму вместе с комиссией
          let balance = users[i].cards[j].balance - totalAmount; //Получил итоговый баланс

          if (users[i].cards[j].credit === true) { //Если кредит дозволен
            return (users[i].cards[j].limit * -1 <= balance)
          }

          return (balance >= 0) //Если кредит запрещен
          }
        }
      }
  }

  //Убирает лишний текст на экране банкомата
  _hideExcess() {
    mainAtmMess.textContent = '';
    atmUserInput.textContent = '';

    leftAtmMsg1.textContent = '';
    leftAtmMsg2.textContent = '';
    leftAtmMsg3.textContent = '';
    leftAtmMsg4.textContent = '';

    rightAtmMsg1.textContent = '';
    rightAtmMsg2.textContent = '';
    rightAtmMsg3.textContent = '';
    rightAtmMsg4.textContent = '';
  }

  //Убирает прошлые события с кнопок банкомата, когда переключаются его окна
  _removeListeners() {
    switch (atmCurrentWindow) {
      case 'errorScreen' : {
        atmBtnEnter.removeEventListener('click', this._continueErrorListener);
        atmBtnCancel.removeEventListener('click', this._continueErrorListener);
        rightAtmBtn4.removeEventListener('click', this._continueErrorListener);

        break;
      }

      case 'successScreen' : {
        atmBtnEnter.removeEventListener('click', this._goToOptionsListener);
        atmBtnCancel.removeEventListener('click', this._goToOptionsListener);
        rightAtmBtn4.removeEventListener('click', this._goToOptionsListener);

        break;
      }

      case 'startScreen' : {
        mainAtmBtn0.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn1.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn2.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn3.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn4.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn5.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn6.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn7.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn8.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn9.removeEventListener('click', this._btnsCodeListener);

        atmBtnClear.removeEventListener('click', this._clearCodeListener);

        rightAtmBtn4.removeEventListener('click', this._enterCodeListener);
        atmBtnEnter.removeEventListener('click', this._enterCodeListener);

        break;
      }

      case 'optionsScreen' : {
        leftAtmBtn3.removeEventListener('click', this._showBalanceListener);
        leftAtmBtn4.removeEventListener('click', this._withdrawListener);
        rightAtmBtn3.removeEventListener('click', this._changePhoneListener);
        rightAtmBtn4.removeEventListener('click', this._endWorkListener);

        break;
      }

      case 'balanceScreen': {
        rightAtmBtn4.removeEventListener('click', this._goToOptionsListener);
        atmBtnCancel.removeEventListener('click', this._goToOptionsListener);

        break;
      }

      case 'changePhoneScreen' : {
        mainAtmBtn0.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn1.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn2.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn3.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn4.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn5.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn6.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn7.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn8.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn9.removeEventListener('click', this._btnsCodeListener);

        atmBtnClear.removeEventListener('click', this._clearCodeListener);

        rightAtmBtn3.removeEventListener('click', this._goToOptionsListener);
        atmBtnCancel.removeEventListener('click', this._goToOptionsListener);

        rightAtmBtn4.removeEventListener('click', this._enterPhoneCodeListener);
        atmBtnEnter.removeEventListener('click', this._enterPhoneCodeListener);

        break;
      }

      case 'withdrawScreen' : {
        mainAtmBtn0.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn1.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn2.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn3.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn4.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn5.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn6.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn7.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn8.removeEventListener('click', this._btnsCodeListener);
        mainAtmBtn9.removeEventListener('click', this._btnsCodeListener);

        atmBtnClear.removeEventListener('click', this._clearCodeListener);

        rightAtmBtn3.removeEventListener('click', this._goToOptionsListener);
        atmBtnCancel.removeEventListener('click', this._goToOptionsListener);

        rightAtmBtn4.removeEventListener('click', this._withdrawCashListener);
        atmBtnEnter.removeEventListener('click', this._withdrawCashListener);
        leftAtmBtn1.removeEventListener('click', this._withdrawCashListener);
        leftAtmBtn2.removeEventListener('click', this._withdrawCashListener);
        leftAtmBtn3.removeEventListener('click', this._withdrawCashListener);
        leftAtmBtn4.removeEventListener('click', this._withdrawCashListener);
        rightAtmBtn1.removeEventListener('click', this._withdrawCashListener);
        rightAtmBtn2.removeEventListener('click', this._withdrawCashListener);

        break;
      }
    }
  }



  //Публичные методы:

  //Запуск банкомата
  startAtm() {
    this._hideExcess();
    this._removeListeners();
    this._showStartScreen();
  }

  //Просмотр баланса
  checkBalance(code) {
    for (let i = 0; i < users.length; i++) {
      for (let j = 0; j < users[i].cards.length; j++) {
        if (users[i].cards[j].cardNumber == code) {
          return users[i].cards[j].balance;
        }
      }
    }
  }

  //Снятие наличных
  withdrawCash(code, amount) {
    for (let i = 0; i < users.length; i++) {
      for (let j = 0; j < users[i].cards.length; j++) {
        if (users[i].cards[j].cardNumber == code) { //Нашли нужного пользователя
          let totalCommission = users[i].cards[j].commission + this.commission; //Посчитал общую комиссию
          let totalAmount = amount + (amount / 100 * totalCommission); //Посчитал выводимую сумму вместе с комиссией
          users[i].cards[j].balance = users[i].cards[j].balance - totalAmount; //Получил итоговый баланс
          }
        }
      }
  }

  //Смена номера телефона
  changePhone(code, newNumber) {
    for (let i = 0; i < users.length; i++) {
      for (let j = 0; j < users[i].cards.length; j++) {
        if (users[i].cards[j].cardNumber == code) {
          users[i].phoneNumber = newNumber;
          return;
        }
      }
    }
  }

  //Закончить работу с банкоматом
  endWorkAtm() {
    this._hideExcess();
    this._removeListeners();

    atmPrevWindow = null;
    atmCurrentWindow = null;
    currentCard = null;
    currentAtm = null;
  }
}

let alphaBank = new СreateATM(1, 'Альфа банк', 'Ул. Пушкина 2', 3);
let privatBank = new СreateATM(2, 'Приват банк', 'Ул. Пушкина 3', 1);
let ukrBank = new СreateATM(3, 'Укр банк', 'Ул. Пушкина 3', 10);
let myBank = new СreateATM(4, 'Мой банк', 'Ул. Пушкина 3', 9);

refreshCardsTable(); //Обновляем таблицу карточек


//Событие для смены банкомата
changeAtmBtn.addEventListener('click', evt => {
  evt.preventDefault();

  let atmId = +selectAtm.value;

  if (!atmId) {
    alert('Выберите существующий банкомат!');
    return;
  }

  (currentAtm) ? currentAtm.endWorkAtm() : {};

  for (let i = 0; i < atms.length; i++) {
    if (atms[i].id === atmId) {
      atms[i].startAtm();
      return;
    }
  }
});

//Функция для обновления доступных банкоматов, исполняется при создании нового экземпляра класса CreateATM
function refreshAvailableAtm() {
  let newAtm = atms[atms.length - 1];

  let opt = document.createElement('option');
  opt.textContent = `${newAtm.name}, ${newAtm.address}, комиссия: ${newAtm.commission}%`;
  opt.value = '' + newAtm.id;

  selectAtm.appendChild(opt);
}

//События для переключения модального окна карточек
cardsInfo.addEventListener('click', toggleModalCards);
overlay.addEventListener('click', toggleModalCards);
closeCardsModalBtn.addEventListener('click', toggleModalCards);

function toggleModalCards(evt) {
  evt.preventDefault();

  cardsModal.classList.toggle('overlay--show');
  overlay.classList.toggle('overlay--show');
  closeCardsModalBtn.classList.toggle('overlay--show');
}

//Обновление таблицы карточек
refreshCardsBtn.addEventListener('click', evt => {
  evt.preventDefault();

  refreshCardsTable();
})

function refreshCardsTable() {
  let oldTbody = cardsTable.querySelector('tbody');
  (oldTbody) ? oldTbody.remove() : {};

  let tbody = document.createElement('tbody');

  for (let i = 0; i < users.length; i++) {
    let phoneNumber = users[i].phoneNumber;

    for (let j = 0; j < users[i].cards.length; j++) {
      let cardNumber = users[i].cards[j].cardNumber;
      let commission = users[i].cards[j].commission;
      let balance = users[i].cards[j].balance;
      let limit = (users[i].cards[j].credit) ? users[i].cards[j].limit : null;

      let tr = document.createElement('tr');

      let td1 = document.createElement('td');
      td1.textContent = `${cardNumber.slice(0, 4)} ${cardNumber.slice(4, 8)} ${cardNumber.slice(8, 12)} ${cardNumber.slice(12, 16)}`;

      let td2 = document.createElement('td');
      td2.textContent = `${commission}%`;

      let td3 = document.createElement('td');
      td3.textContent = (limit) ? `${limit}` : `0`;

      let td4 = document.createElement('td');
      td4.textContent = `${phoneNumber}`;

      let td5 = document.createElement('td');
      td5.textContent = `${balance} грн.`;

      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      tr.appendChild(td4);
      tr.appendChild(td5);

      tbody.appendChild(tr);
    }
  }

  cardsTable.appendChild(tbody);
}
