/**
 * For starting - create car with consumption:
 * var car = Car(5, 2, 180);
 *
 * Car module. Created car with custom consumption.
 * @param consumption {number} gas per 100 km
 * @param oil {number}  In milliliters per 100 km
 * @param maxSpeed {number} Km per hour
 * @return {{
 *            start: start,
 *            riding: riding,
 *            checkGas: checkGas,
 *						checkOil: checkOil,
 *            refueling: refueling,
 * 						refuelingOil: refuelingOil
 *           }}
 * @constructor
 */
function Car(consumption, oil, maxSpeed) {
  if (!isNumeric(consumption) || consumption <= 0) {
    showMessage('Wrong consumption', 'error');
    return;
	}
	
	if (oil !== undefined && !isNumeric(oil) || oil <= 0) {
		showMessage('Wrong oil consumption', 'error');
		return;
	}

	if (maxSpeed !== undefined && !isNumeric(maxSpeed) || maxSpeed <= 0) {
		showMessage('Wrong max speed', 'error');
		return;
	}

  let gasBalance = 100;
	let gasConsumption = consumption / 100;
	let oilConsumption = oil / 100;
	let oilBalance = 1000; // В миллилитрах
	let gasResidue;
	let oilResidue;
	let gasVolume = 200;
	let oilVolume = 2000;
  let ignition = false;
  let ready = false;

  /**
	 * Check gas amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Gas amount.
	 */
  function gasCheck(distance, coefficientForSpeed) {
    if (gasBalance <= 0) {
      return 0;
    }

    let gasForRide = Math.round(distance * gasConsumption * coefficientForSpeed);

    return gasBalance - gasForRide;
	}
	
	 /**
	 * Check oil amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Oil amount.
	 */
  function oilCheck(distance) {
    if (oilBalance <= 0) {
      return 0;
    }

    let oilForRide = Math.round(distance * oilConsumption);

    return oilBalance - oilForRide;
  }

  /**
	 * Show message for a user in the console.
	 * @param message {string} - Text message for user.
	 * @param type {string} - Type of the message (error, warning, log). log by default.
	 */
  function showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        console.error(messageText);
        break;
      case 'warning':
        console.warn(messageText);
        break;
      case 'log':
        console.log(messageText);
        break;
      default:
        console.log(messageText);
        break;
    }
  }

  /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */
  function checkRideGas(distance, coefficientForSpeed) {
    gasResidue = gasCheck(distance, coefficientForSpeed);
  }

	 /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */
  function checkRideOil(distance) {
    oilResidue = oilCheck(distance);
	}
	
  /**
	 * Check value to number.
	 * @param n {void} - value.
	 * @return {boolean} - Is number.
	 */
  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  /**
	 * Public methods object.
	 */
  return {
    /**
		 * Start car.
		 */
    start: function() {
      ignition = true;

      if (gasBalance <= 0) {
        showMessage('You don\'t have gas. Please refuel the car.', 'error');
        ready = false;
        return;
			}
			
			if (oilBalance <= 0) {
				showMessage('You don\'t have oil. Please refuel the car.', 'error');
        ready = false;
        return;
			}

      ready = true;

      showMessage('Ingition', 'log');
    },

    /**
		 * Riding function.
		 * @param distance {number} - Riding distance.
		 * @param speed {number} - Riding speed, not necessary.
		 */
    riding: function(distance, speed) {
      if (!isNumeric(distance) || distance <= 0) {
        showMessage('Wrong distance', 'error');
        return;
			}
			
			if (speed !== undefined && !isNumeric(speed) || speed <= 0 || speed > maxSpeed) {
        showMessage('Wrong speed', 'error');
        return;
			}

      if (!ignition) {
        showMessage('You need start car', 'error');
        return;
      }

      if (!ready) {
        ignition = false;
        showMessage('You need gas/oil station', 'error');
        return;
			}

			if (gasBalance === 0 || oilBalance === 0) {
				showMessage('You need gas/oil station', 'error');
				ignition = false;
				return;
			}

			let coefficientForSpeed = 1; // По умолчанию коеф. за скорость 1
			let TimeDriven;

			if (speed !== undefined) { //Если скорость задали
				coefficientForSpeed = speed / 100 + 0.3; // Не знаю какой формулой вычислять пропорционально от скорости коефициент. Поэтому сделал так
				//TimeDriven = +(distance / speed).toFixed(1);
			}

			checkRideGas(distance, coefficientForSpeed);
			let gasDriven = Math.round(gasBalance / gasConsumption / coefficientForSpeed);
			let oilDriven;

			if (oil !== undefined) { //Если расход масла задали
				checkRideOil(distance);
				oilDriven = Math.round(oilBalance / oilConsumption);
			}

      if (oilResidue < 0) { // Если масло закончилось
				if (gasResidue < 0 && oilDriven > gasDriven) { // Если бензин первее закончился
					let distanceLeftGas = Math.round(distance - gasDriven);
					checkRideOil(gasDriven);
					oilBalance = oilResidue;
					let neddedGas = distanceLeftGas * gasConsumption * coefficientForSpeed;
					gasBalance = 0;

					showMessage('Gas over. You have driven - ' + gasDriven + ' km. You need ' + neddedGas + ' liters. ' + distanceLeftGas + 'km left', 'warning');
					showMessage('Oil balance - ' + oilBalance, 'log');

					if (speed) {
						TimeDriven = +(gasDriven / speed).toFixed(1);
						showMessage('Time driven: ' + TimeDriven + ' hours', 'log');
					}

					return;
				}

				// Если первее масло закончилось
				let distanceLeftOil = Math.round(distance - oilDriven);
				checkRideGas(oilDriven, coefficientForSpeed);
				oilBalance = 0;
				let neddedOil = distanceLeftOil * oilConsumption;
				gasBalance = gasResidue;

				showMessage('Oil over. You have driven - ' + oilDriven + ' km. You need ' + neddedOil + ' milliliters. ' + distanceLeftOil + 'km left', 'warning');
				showMessage('Gas balance - ' + gasBalance, 'log');

				if (speed) { // Если скорость задали
					TimeDriven = +(oilDriven / speed).toFixed(1);
					showMessage('Time driven: ' + TimeDriven + ' hours', 'log');
				}

				return;
			}

      if (gasResidue < 0) { // Если бензин закончился
        let distanceLeftGas = Math.round(distance - gasDriven);
        gasBalance = 0;
        let neddedGas = distanceLeftGas * gasConsumption * coefficientForSpeed;

				showMessage('Gas over. You have driven - ' + gasDriven + ' km. You need ' + + neddedGas + ' liters. ' + distanceLeftGas + 'km left', 'warning');

				if (oil !== undefined) { // Если масло задали
					checkRideOil(gasDriven);
					oilBalance = oilResidue;
					showMessage('Oil balance - ' + oilBalance, 'log');
				}

				if (speed) { // Если скорость задали
					TimeDriven = +(gasDriven / speed).toFixed(1);
					showMessage('Time driven: ' + TimeDriven + ' hours', 'log');
				}

				return;
      }

      if (gasResidue >= 0) { // Если бензи не закончился
        gasBalance = gasResidue;
				showMessage('You arrived. Gas balance - ' + gasResidue, 'log');
				
				if (oil !== undefined) { // Если масло задали
					oilBalance = oilResidue;
					showMessage('Oil balance - ' + oilBalance, 'log');
				}

				if (speed) { // Если скорость задали
					TimeDriven = +(distance / speed).toFixed(1);
					showMessage('Time driven: ' + TimeDriven + ' hours', 'log');
				}

				return;
			}
    },

    /**
		 * Check gas function.
		 */
    checkGas: function() {
			showMessage('Gas - ' + gasBalance, 'log');
		},
		
		/**
		 * Check oil function.
		 */
		checkOil: function() {
			if (oil === undefined) {
				showMessage('You didnt specify oil consumption, cant count', 'error');
			} else {
				showMessage('Oil - ' + oilBalance, 'log');
			}
    },

    /**
		 * Refueling function.
		 * @param gas
		 */
    refueling: function(gas) {
      if (!isNumeric(gas) || gas <= 0) {
        showMessage('Wrong gas refuel amount', 'error');
        return;
      }

      if (gasVolume === gasBalance) {
        showMessage('Gasoline tank is full', 'warning');
      } else if (gasVolume < gasBalance + gas) {
        let excess = Math.round(gas - gasVolume - gasBalance);
				showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
				gasBalance = gasVolume;
      } else {
        gasBalance = Math.round(gasBalance + gas);
        showMessage('Gas balance - ' + gasBalance, 'log');
      }
		},
		
		 /**
		 * Refueling oil function.
		 * @param oil
		 */
		refuelingOil: function(oilRefuel) {
			if (oil === undefined) {
				showMessage('You didnt specify oil consumption, cant refuel', 'error');
				return;
			}
			
			if (!isNumeric(oilRefuel) || oilRefuel <= 0) {
        showMessage('Wrong oil refuel amount', 'error');
        return;
      }

      if (oilVolume === oilBalance) {
        showMessage('Oil tank is full', 'warning');
      } else if (oilVolume < oilBalance + oilRefuel) {
        let excess = Math.round(oilRefuel - oilVolume - gasBalance);
				showMessage('Oil tank is full. Excess - ' + excess, 'log');
				oilBalance = oilVolume;
      } else {
        oilBalance = Math.round(oilBalance + oilRefuel);
        showMessage('Oil balance - ' + oilBalance, 'log');
      }
    },
  };
}
